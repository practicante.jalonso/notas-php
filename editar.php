<?php
/**
 * @var PDO $db
 */
session_start();

if (!isset($_SESSION['nombre'])) {
    header('Location: login.php');
}
if (!isset($_GET['id'])) {
    header('Location: index.php');
}

include('model/conexion.php');

$id = $_GET['id'];

$sentencia = $db->prepare('SELECT * FROM alumno WHERE id_alumno = ?;');
$sentencia->execute([$id]);
$persona = $sentencia->fetch(PDO::FETCH_OBJ);
?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Edit Student</title>
</head>
<body>
<h3>
    Edit Student
</h3>
<form method="POST" action="editarProceso.php?">
    <table>
        <tr>
            <td><label>Fathers last name
                    <input type="text" name="newTxtFathers" value="<?php echo $persona->a_paterno; ?>">
                </label>
            </td>
        </tr>
        <tr>
            <td><label>Mothers last name
                    <input type="text" name="newTxtMothers" value="<?php echo $persona->a_materno; ?>">
                </label></td>
        </tr>
        <tr>
            <td><label>Full name
                    <input type="text" name="newTxtName" value="<?php echo $persona->nombre; ?>">
                </label>
            </td>
        </tr>
        <tr>
            <td><label>Midterm result
                    <input type="text" name="newTxtMidterm" value="<?php echo $persona->ex_parcial; ?>">
                </label>
            </td>
        </tr>
        <tr>
            <td><label>Final result
                    <input type="text" name="newTxtFinal" value="<?php echo $persona->ex_final; ?>">
                </label>
            </td>
        </tr>
        <tr>
            <input type="hidden" name="id_alumno" value="<?php echo $persona->id_alumno ?>">
            <td><input type="submit" value="Apply"></td>
        </tr>
    </table>
</form>
</body>
</html>
