<?php
    session_start();
    if(isset($_SESSION['nombre'])) {
        header('Location: index.php');
    }
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>login</title>
</head>
<body>
<form method="POST" action="loginProceso.php">
    <label>Username
        <input type="text" name="txtUser">
    </label>
    <br>
    <label>Password
        <input type="password" name="txtPassword">
    </label>
    <br>
    <input type="submit" value="Login">
</form>
</body>
</html>