<?php
/**
 * @var PDO $db
 */
session_start();

if (!isset($_SESSION['nombre'])) {
    header('Location: login.php');
}

include 'model/conexion.php';

$sentencia = $db->query("SELECT * FROM alumno;");
$alumnos = $sentencia->fetchAll(PDO::FETCH_OBJ);

?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>NOTES!</title>
</head>
<body>
<div style="text-align: center;">
    <h1>📋 Welcome <?php echo $_SESSION['nombre']?> 📎</h1>
    <a href="logout.php">logout</a>
    <h3>Student Reports</h3>
</div>
<table>
    <tr>
        <td>Code</td>
        <td>Last names</td>
        <td>Names</td>
        <td>Midterm</td>
        <td>Final</td>
        <td>Average</td>
        <td>Edit</td>
        <td>Eliminar</td>
    </tr>
    <?php
    foreach ($alumnos as $alumno) {
        ?>
        <tr>
            <td><?php echo $alumno->id_alumno; ?></td>
            <td><?php echo $alumno->a_paterno . ' ' . $alumno->a_materno; ?></td>
            <td><?php echo $alumno->nombre; ?></td>
            <td><?php echo $alumno->ex_parcial; ?></td>
            <td><?php echo $alumno->ex_final; ?></td>
            <td><?php echo ($alumno->ex_final + $alumno->ex_parcial) / 2; ?></td>
            <td><a href="editar.php?id=<?php echo $alumno->id_alumno; ?>">✏️</a></td>
            <td><a href="eliminar.php?id=<?php echo $alumno->id_alumno; ?>">⛔</a></td>
        </tr>
        <?php
    }
    ?>
</table>
<!--Table end-->

<hr>

<!--Beggining for insert-->
<h3>
    Register Student
</h3>
<form action="insert.php" method="post">
    <table>
        <tr>

            <td><label>Father's last name
                    <input type="text" name="txtFathers">
                </label></td>
        </tr>
        <tr>

            <td><label>Mother's last names
                    <input type="text" name="txtMothers">
                </label></td>
        </tr>
        <tr>

            <td><label>Full Name
                    <input type="text" name="txtName">
                </label></td>
        </tr>
        <tr>
            <td><label>Midterm
                    <input type="text" name="txtMidterm">
                </label></td>
        </tr>
        <tr>
            <td><label>Final
                    <input type="text" name="txtFinal">
                </label></td>
        </tr>
        <input type="hidden" name="oculto" value="1">
        <tr>
            <td><input type="reset" name=""></td>
            <td><input type="submit" value="Register"></td>
        </tr>
    </table>
</form>
<!--End of insert-->

</body>
</html>