<?php
session_start();
try {
    session_destroy();
    header('Location: login.php');
} catch (Exception $e) {
    echo "Error de conexion " . $e->getMessage();
}