<?php
/**
 * @var PDO $db
 */
session_start();

if (!isset($_SESSION['nombre'])) {
    header('Location: login.php');
}
if (!isset($_GET['id'])) {
    die;
}

include('model/conexion.php');

$id = $_GET['id'];


try {
    $statement = $db->prepare("DELETE FROM alumno WHERE id_alumno = ?");
    $statement->execute([$id]);
    header('Location: index.php');
} catch (Exception $e) {
    echo "Error de conexion " . $e->getMessage();
}