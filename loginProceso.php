<?php
/**
 * @var PDO $db
 */

session_start();

include_once 'model/conexion.php';

$username = $_POST['txtUser'];
$password = $_POST['txtPassword'];

try {
    $statement = $db->prepare("SELECT * FROM usuario WHERE nombre = ? AND password = ?");
    $statement->execute([$username, $password]);
    $data = $statement->fetch(PDO::FETCH_OBJ);

    if ($data === false) {
        header('Location: login.php');
    } elseif ($statement->rowCount() == 1) {
        echo $_SESSION['nombre'] = $data->nombre;
        header('Location: index.php');
    }

//    print_r($data);
//    header('Location: index.php');
} catch (Exception $e) {
    echo "Error de conexion " . $e->getMessage();
}