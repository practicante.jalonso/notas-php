<?php
/**
 * @var PDO $db
 */
session_start();

if (!isset($_SESSION['nombre'])) {
    header('Location: login.php');
}
if (!isset($_POST['oculto'])) {
    die;
}

include 'model/conexion.php';

$a_paterno = $_POST['txtFathers'];
$a_materno = $_POST['txtMothers'];
$nombre = $_POST['txtName'];
$ex_parcial = $_POST['txtMidterm'];
$ex_final = $_POST['txtFinal'];

try {
    $statement = $db->prepare("INSERT INTO alumno(a_paterno, a_materno, nombre, ex_parcial, ex_final) VALUES (?,?,?,?,?)");
    $statement->execute([$a_paterno, $a_materno, $nombre, $ex_parcial, $ex_final]);
    header('Location: index.php');
} catch (Exception $e) {
    echo "Error de conexion " . $e->getMessage();
}